Package package = Package.Current;
PackageId packageId = package.Id;
PackageVersion version = packageId.Version;
string version = String.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);